# CodeIgniter 4 
## 1. Setup
> composer install

复制env 文件至 .env 进行环境变量配置

## 2. Run migration
打开终端并导航到您的项目文件夹，然后运行：
> php spark make:migration Users
> php spark migrate 


这将在您的数据库中创建您在 .env 文件中指定的表“user”
### 3. 配置nginx重写指令(`try_files`) 
```sh
        location / {
            index index.php index.html error/index.html;
            try_files $uri $uri/ /index.php/$args;
            autoindex  off;
        }
```
### 4. 现在您可以注册您的第一个用户
