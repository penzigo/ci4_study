<?php namespace App\Models;

use CodeIgniter\Model;

class AvatarModel extends Model{
  protected $table = 'avatar';
  protected $allowedFields = ['img_name','file'];
}
