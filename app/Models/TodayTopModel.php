<?php namespace App\Models;

use CodeIgniter\Model;

class TodayTopModel extends Model{
  protected $table = 'today_top';
  protected $allowedFields = ['event_day','title','content','month_day','avatar'];
}
