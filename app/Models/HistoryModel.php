<?php namespace App\Models;

use CodeIgniter\Model;

class HistoryModel extends Model{
  protected $table = 'history2';
  protected $allowedFields = ['event_day','title','content','month_day','avatar'];
}
