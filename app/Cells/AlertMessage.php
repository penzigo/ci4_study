<?php 

namespace App\Cells;

use CodeIgniter\View\Cells\Cell;

// https://codeigniter.com/user_guide/outgoing/view_cells.html#presentation-methods

class AlertMessage extends Cell
{
    public $type;
    public $message;

    //您可以通过在类中设置视图属性来指定自定义视图名称。视图将像任何视图通常一样定位。
    // protected $view = 'my/custom/view';

    /*
    自定义渲染
        如果您需要更多地控制 HTML 的呈现，您可以实现 render() 方法。
        如果需要，此方法允许您执行额外的逻辑并向视图传递额外的数据。
        render() 方法必须返回一个字符串。
        要利用受控单元的全部功能，您应该使用 $this->view() 而不是普通的 view() 辅助函数。
    */
//     public function render(): string
//     {
//         return $this->view('my/custom/view', ['extra' => 'data']);
//     }
}
