<div class='container'>
<input type="checkbox" class="custom-control-input" id="allowmultiple">
    <label class="custom-control-label" for="allowmultiple" style="cursor: pointer;">
        Multiple Select
    </label>
    <!-- <div id="selectedmediapreview"></div> -->
    <input type="text" name='avatar' id='selectedmediapreview' value="<?= $selected_ids ?>">
    <div class='row'>
        <?php foreach ($image_list as $value) : ?>
            <div class='col-auto'>
                <label class="image-checkbox">
                    <img su-media-id="<?= $value['id'] ?>" src="<?= "{$image_path}{$value['img_name']}" ?>" />
                    <i class="fa fa-check"></i>
                </label>
            </div>
        <?php endforeach ?>
    </div>

</div>

<style>
    .image-checkbox {
        cursor: pointer;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        border: 3px solid transparent;
        box-shadow: 0 0 4px #ccc;
        outline: 0;
        margin: 4px;
        border-radius: 12px;
    }

    .image-checkbox-checked {
        border-color: #2196f3;
    }

    img {
        border-radius: 8px;
        max-height: 160px !important;
        max-width: -webkit-fill-available;
    }

    .image-checkbox i {
        display: none;
        color: #2196f3;
    }

    .image-checkbox-checked {
        position: relative;
    }

    .image-checkbox-checked i {
        display: block;
        position: absolute;
        top: 10px;
        right: 10px;
    }
</style>

<script>
    function init_load(){
        var ids = $('#selectedmediapreview').val().split(",")
        console.log('ids',ids)
        ids.forEach(id => {
            if(id){
                console.log('id',id)
                $(`img[su-media-id=${id}]`).parent().addClass('image-checkbox-checked');
            }
        });
    }
    jQuery(function($) {
        var mediaArray = [];
        var selectedMediasId;
        var isMultipleAllowed = false;
        init_load()
        $('#allowmultiple').click(function() {
            isMultipleAllowed = $('#allowmultiple').is(':checked') ? true : false;
            $('.image-checkbox-checked').each(function() {
                $(this).removeClass('image-checkbox-checked');
            });
            mediaArray = [];
            // $('#selectedmediapreview').html('');
            $('#selectedmediapreview').val('');
        });
        $(".image-checkbox").on("click", function(e) {
            var selected = $(this).find('img').attr('su-media-id');
            //console.log(selected);
            if ($(this).hasClass('image-checkbox-checked')) {
                $(this).removeClass('image-checkbox-checked');
                // remove deselected item from array
                mediaArray = $.grep(mediaArray, function(value) {
                    return value != selected;
                });
            } else {
                if (isMultipleAllowed == false) {
                    $('.image-checkbox-checked').each(function() {
                        $(this).removeClass('image-checkbox-checked');
                    });
                    mediaArray = [];
                    mediaArray.push(selected);
                } else {
                    if (mediaArray.indexOf(selected) === -1) {
                        mediaArray.push(selected);
                    }
                }
                $(this).addClass('image-checkbox-checked');
            }
            //console.log(selected);
            console.log(mediaArray);
            selectedMediasId = mediaArray.join(",");
            console.log(selectedMediasId);
            // $('#selectedmediapreview').html('<div class="alert alert-success"><pre lang="js">' + JSON.stringify(mediaArray.join(", "), null, 4) + '</pre></div>');
            $('#selectedmediapreview').val( mediaArray.join(","))
            //console.log(isMultipleAllowed);
            e.preventDefault();
        });
    });
</script>