<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
// $routes->setDefaultController('Users');
$routes->setDefaultMethod('index');
// $routes->setTranslateURIDashes(false);
$routes->setTranslateURIDashes(true);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
// $routes->get('/', 'Home::index');
// $routes->match(['get','post'],'/', 'Home::index', ['filter' => 'noauth','as'=>'home','cache'=>300]);
// $routes->match(['get','post'],'/', 'Home::index', ['as'=>'home']);
$routes->get('/', 'Home::index');
$routes->get('/history/(\d{2})(\d{2})\.html', 'Home::history/$1/$2/');
// $routes->get('h/(:num)/(:num)', 'Home::index', ['filter' => 'noauth','as'=>'home','cache'=>300]);
$routes->match(['get','post'],'login/', 'Users::index', ['filter' => 'noauth']);
$routes->get('logout/', 'Users::logout');
// $routes->get('register', 'Users::register');
$routes->match(['get','post'],'register/', 'Users::register', ['filter' => 'noauth']);
$routes->match(['get','post'],'profile/', 'Users::profile',['filter' => 'auth']);
$routes->group('admin', function($routes)
{
        $routes->get('/', 'Dashboard::index',['filter' => 'auth']);
        $routes->get('upload/index', 'AjaxFileUpload::index');
        $routes->post('upload', 'AjaxFileUpload::upload');
        $routes->group('history', function($routes)
        {
                $routes->match(['get','post'],'/', 'History::list',['filter' => 'auth']);
                $routes->delete('delete/(:num)', 'History::delete/$1',['filter' => 'auth']);
                $routes->match(['get','post'],'create', 'History::create',['filter' => 'auth']);
                $routes->match(['get','post'],'update/(:num)', 'History::update/$1',['filter' => 'auth']);
        });
        $routes->group('today_top', function($routes)
        {
                $routes->match(['get','post'],'/', 'TodayTop::list',['filter' => 'auth']);
                $routes->delete('delete/(:num)', 'TodayTop::delete/$1',['filter' => 'auth']);
                $routes->match(['get','post'],'create', 'TodayTop::create',['filter' => 'auth']);
                $routes->match(['get','post'],'update/(:num)', 'TodayTop::update/$1',['filter' => 'auth']);
        });

});
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
