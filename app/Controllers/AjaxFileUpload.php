<?php

namespace App\Controllers;

// use CodeIgniter\Controller;
use App\Models\AvatarModel;

// class AjaxFileUpload extends Controller
class AjaxFileUpload extends BaseController
{
    protected $model;
    private $UPLOAD_PATH = ROOTPATH . 'public/assets/';
    public function __construct()
    {
        $this->model = new AvatarModel();
    }
    public function index()
    {
        echo view('templates/header');
        echo view('admin/upload');
        echo view('templates/footer');
    }

    public function upload()
    {
        helper(['form', 'url']);


        $validateImage = $this->validate([
            'file[]' => [
                'uploaded[file]',
                // 'mime_in[file, image/png, image/x-png, image/jpg, image/jpeg, image/gif]',
                'max_size[file, 4096]',
                'is_image[file]'
            ],
        ]);

        if (!$validateImage) {
                    // $validateImage = true;
            $response = [
                'success' => false,
                'data' => '',
                'msg' => "Image could not upload"
                // 'msg' => $this->validator
            ];
            return  $this->response->setJSON($response);
        }
        // {
        // $imageFile = $this->request->getFile('file');            
        $files = $this->request->getFileMultiple('file');     
        // var_export($files);
        // exit();
        if($files){
            foreach ($files as $imageFile ) {
                # code...
                $imageFile->move($this->UPLOAD_PATH . 'uploads');
                // $imageFile->move(WRITEPATH . 'uploads');
                $data = [
                    'img_name' => $imageFile->getClientName(),
                    'file'  => $imageFile->getClientMimeType(),
                ];
                $save = $this->model->insert($data);
            }
            $response = [
                'success' => true,
                'data' => $save,
                'msg' => "Image successfully uploaded"
            ];
        }       
        // }
        return $this->response->setJSON($response);
    }
}
