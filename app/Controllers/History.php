<?php

namespace App\Controllers;

use App\Models\HistoryModel;
use App\Models\AvatarModel;
use App\Controllers\Admin;

class History extends Admin
{
	protected $list_display =  ['id', 'event_day', 'title', 'content', 'month_day', 'img_name'];
	protected $search_fields = ['title', 'content', 'month_day'];
	protected $filter_fields = ['event_day' => 'date'];
	protected $form_fields = ['title', 'content', 'event_day', 'month_day', 'avatar'];
	protected function get_model()
	{
		return new HistoryModel();
	}
	protected function list_filter()
	{
		$qs = parent::list_filter();
		$month = $this->request->getGet('month');
		$day = $this->request->getGet('day');
		if ($month && $day) {
			$vm = str_pad($month, 2, "0", STR_PAD_LEFT);
			$vd = str_pad($day, 2, "0", STR_PAD_LEFT);
			return $qs->where('month_day', "{$vm}_{$vd}");
		}
		return $qs;
	}
	protected function get_model_data()
	{
		$newData = parent::get_model_data();
		$date = strtotime($newData['event_day']);
		$newData['month_day'] = date('m_d', $date);
		return $newData;
	}

	protected function get_validate_rules()
	{
		$rules = [
			'event_day' => 'required|valid_date[Y-m-d]',
			'title' => 'required|min_length[3]|max_length[200]',
			'content' => 'required|min_length[6]|max_length[500]',
		];
		return $rules;
	}


	protected function get_queryset()
	{
		$qs = parent::get_queryset();
		$qs->select('history2.id,event_day,title,content,month_day,avatar.img_name')->join('avatar', 'avatar.id=history2.avatar', 'left');
		return $qs;
	}
	//--------------------------------------------------------------------

	protected function get_update_data()
	{
		$data = parent::get_update_data();
		$m = new AvatarModel();
		$data['image_list'] = $m->select('id,img_name')->findAll();
		$data['image_path'] = '/assets/uploads/';
		return $data;
	}

	// function get_create_success_redirect_url()
	// {
	// 	return route_to('History::list');
	// }

	// function get_update_success_redirect_url()
	// {
	// 	return route_to('History::list');
	// }
}
