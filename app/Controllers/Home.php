<?php

namespace App\Controllers;

use App\Models\HistoryModel;
use App\Models\TodayTopModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

// use CodeIgniter\HTTP\RequestInterface;
// use CodeIgniter\HTTP\ResponseInterface;
// use Psr\Log\LoggerInterface;

class Home extends BaseController
{
    private $model;
    // private $parser;
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        $this->model = new HistoryModel();
    }
    protected function get_queryset()
    {
        return $this->model->select('title,content,avatar.img_name')->join('avatar', 'avatar.id=history2.avatar', 'left');
    }
    public function index()
    {
        # http://cpc.people.com.cn/GB/64162/64165/76621/76622/index.html

        $text_search = $this->request->getVar('names');
        $vm = '';
        $vd = '';
        if ($text_search) {
            $qs = $this->get_queryset()->like('title', $text_search)->orLike('content', $text_search)->orderBy('event_day');
            $title =  "搜索关于_{$text_search}_资料中心";
            $data = [
                'object_list' => $qs->paginate(10),
                'pager' => $qs->pager->links(),
                'today_top' => null,
                'text_search' => $text_search,
                'vm' => $vm,
                'vd' => $vd,
            ];
        } else {

            $event_day = getdate();
            $vm = str_pad($event_day['mon'], 2, "0", STR_PAD_LEFT);
            $vd = str_pad($event_day['mday'], 2, "0", STR_PAD_LEFT);
            $month_day =  "{$vm}_{$vd}";
            $qs = $this->get_queryset()->where('month_day', $month_day);
            // $today_top_model = new TodayTopModel();
            // $today_top=$today_top_model->select('title,content,avatar.img_name')->join('avatar','avatar.id=history2.avatar','left')->where('month_day', $month_day)->first();
            $lm = ltrim($vm, "0");
            $ld = ltrim($vd, "0");
            $title =  "{$lm}日{$ld}日--资料中心";
            $data = [
                'object_list' => $qs->findAll(),
                'today_top' => $this->_todaytop($month_day),
                'pager' => '',
                'text_search' => $text_search,
                'vm' => $vm,
                'vd' => $vd,
            ];
        }
        // echo view('templates/header_no_nav', ['title' => $title]);
        echo view('templates/header_no_nav', ['title' => $title]);
        // echo $this->parser->setData($data)->render('today');
        echo view('today', $data);
        echo view('templates/footer');
    }

    public function history($vm, $vd)
    {
        $text_search = '';
        $month_day =  "{$vm}_{$vd}";
        $qs = $this->get_queryset()->where('month_day', $month_day);

        $lm = ltrim($vm, "0");
        $ld = ltrim($vd, "0");
        $data = [
            'object_list' => $qs->findAll(),
            'text_search' => $text_search,
            'today_top' => $this->_todaytop($month_day),
            'pager' => '',
            'vm' => $vm,
            'vd' => $vd
        ];
        // echo view('templates/header', ['title' => "{$lm}日{$ld}日--资料中心"]);
        echo view('templates/header_no_nav', ['title' => "{$lm}日{$ld}日--资料中心"]);
        // echo $this->parser->setData($data)->render('today');
        echo view('today', $data);
        echo view('templates/footer');
    }
    private function _todaytop($month_day)
    {
        $today_top_model = new TodayTopModel();
        $today_top = $today_top_model->select('title,content,avatar.img_name')->join('avatar', 'avatar.id=today_top.avatar', 'left')->where('month_day', $month_day)->first();
        return $today_top ? : [];
    }
}
