<?php

namespace App\Controllers;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

use function PHPUnit\Framework\throwException;

/**
 * # 模型查询控制器 CRUD 基础
 * - 增 create()
 * - 查 list()
 * - 改 update($pk)
 * - 删 delete($pk)
 */
class Admin extends BaseController
{
	protected $model;
	protected $list_display = [];
	protected $search_fields = [];
	protected $filter_fields = [];
	protected $form_fields = [];
	protected $ordering;
	protected $pk;
	protected $pagesize_kw = 'pagesize';
	protected $pagesize = 10;
	protected $list_template = 'admin/list';
	protected $update_template = 'admin/update';
	protected $create_template = 'admin/create';
	protected $update_success_redirect_url = '';
	protected $create_success_redirect_url = '';

	public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
	{
		parent::initController($request, $response, $logger);
		$classname = get_class($this);
		$this->update_success_redirect_url = route_to("{$classname}::list");
		$this->create_success_redirect_url = route_to("{$classname}::list");

	}
	/** 
	 * 参数查询函数 ?a=1&b=2&c=3
	 */
	protected function list_filter()
	{
		$qs = $this->get_queryset();
		foreach ($this->filter_fields as $key => $value) {
			# code...
			$v = $this->request->getGet($key);
			if ($v) {
				$qs = $qs->where($key, $v);
			}
		}
		$qs = $this->list_search($qs);
		return $qs;
	}
	/** 
	 * ### 参数搜索函数 ?q=123 
	 * 通过 search_fields 枚举 (a,b,c 在 search_fields 中)
	 * 可以搜索到 a=123 or b=123 or c=123
	 */
	protected function list_search($qs)
	{
		$q = $this->request->getGet('q');
		if ($q) {
			foreach ($this->search_fields as $field) {
				$qs = $qs->orLike($field, $q);
			}
			return $qs;
		}
		return $qs;
	}

	/**获取模型*/
	protected function get_model()
	{
		if (!$this->model) {
			throw new \Exception("Error Processing Model, you should override protected function get_model() ", 1);
		}
		return $this->model;
	}
	/**获取主键*/
	protected function get_pk()
	{
		return $this->pk ?: $this->get_model()->primaryKey;
	}

	/**获取查询语句*/
	protected function get_queryset()
	{
		return $this->get_model();
	}

	/**获取排序规则*/
	protected function get_order()
	{
		return $this->ordering ?: $this->get_model()->primaryKey;
	}
	/**分页参数 */
	protected function get_pagesize()
	{
		$pagesize = $this->request->getGet($this->pagesize_kw);
		return $pagesize ? intval($pagesize) : $this->pagesize;
	}

	protected function get_list_data()
	{
		return [
			'list_display' => $this->list_display,
			'search_fields' => $this->search_fields,
			'filter_fields' => $this->filter_fields,
			'pk' => $this->get_pk(),
			'GET' => $this->request->getGet(),
		];
	}
	/**
	 * CRUD入口 - 查询函数 
	 * */
	public function list()
	{
		helper(['form']);
		$data = $this->get_list_data();
		$qs = $this->list_filter()->orderBy($this->get_order());
		$d2 = [
			'object_list' => $qs->paginate($this->get_pagesize()),
			'pager' => $qs->pager->links()
		];
		echo view('templates/header');
		echo view($this->list_template, array_merge($data, $d2));
		echo view('templates/footer');
	}

	/**CRUD入口 - 删除函数 */
	public function delete($pk)
	{
		$result = $this->get_model()->delete($pk);
		log_message('error', "delete {$this->get_model()->table} on {$this->get_pk()} = {$pk}");

		$result = $this->get_model()->delete(($pk));
		echo "{$result}条记录删除成功";
	}

	protected function get_model_data()
	{
		$newData = [];
		foreach ($this->form_fields as $key) {
			# code...
			$newData[$key] = $this->request->getPost($key);
		}

		return $newData;
	}

	// protected function get_update_data()
	// {
	// 	$newData = [];
	// 	foreach ($this->form_fields as $key) {
	// 		# code...
	// 		$newData[$key] = $this->request->getPost($key);
	// 	}

	// 	return $newData;
	// }
	/**
	 * 表单验证
	 */
	protected function get_validate_rules()
	{
		//let's do the validation here
		return [];
	}

	protected function get_create_data()
	{
		return [];
	}
	protected function get_create_success_redirect_url()
	{
		// if ($this->create_success_redirect_url) {
		// 	return redirect()->to($this->create_success_redirect_url);
		// }
		// return redirect()->to(current_url());
		// return $this->create_success_redirect_url?:current_url();
		return $this->create_success_redirect_url;

	}
	/**
	 * CRUD入口 - 新增函数
	 */
	public function create()
	{
		helper(['form']);
		$data = $this->get_create_data();
		if ($this->request->getPost()) {
			//let's do the validation here
			if (!$this->validate($this->get_validate_rules())) {
				$data['validation'] = $this->validator;
			} else {
				$this->get_model()->save($this->get_model_data());
				$session = session();
				$session->setFlashdata('success', 'Successful Registration');
				// return redirect()->route('Admin::list');
				// return $this->get_create_success_redirect_url();
				return redirect()->to($this->get_create_success_redirect_url());

			}
		}

		echo view('templates/header');
		// echo view('admin/create', $data);
		echo view($this->create_template, $data);
		echo view('templates/footer');
	}


	protected function get_update_data()
	{
		return [];
	}

	protected function get_update_success_redirect_url()
	{
		// return $this->update_success_redirect_url?:current_url();
		return $this->update_success_redirect_url;
	}
	/**
	 * CRUD入口 - 更新函数
	 */
	public function update($pk)
	{

		$data = $this->get_update_data();
		helper(['form']);
		$model = $this->get_model();
		if ($this->request->getPost()) {
			if (!$this->validate($this->get_validate_rules())) {
				$data['validation'] = $this->validator;
			} else {
				$save_data = $this->get_model_data();
				$save_data[$this->get_pk()] = $pk;
				log_message('debug', var_export($save_data, true));
				$model->save($save_data);
				return redirect()->to($this->get_update_success_redirect_url());
			}
		}
		$data['object'] = $model->where($this->get_pk(), $pk)->first();
		echo view('templates/header', $data);
		echo view($this->update_template);
		echo view('templates/footer');
	}
}
