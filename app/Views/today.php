<link rel="stylesheet" href="/today/page.css" type="text/css" media="all" />
<div class=''>
    <!--top-->
    <div class="top01 width884 clear"> <a href="#"><img src="/today/nav01.gif" width="96" height="42" alt=" "></a><br>
        <i><a href="#">
                <font color="#800000"><b>中国共产党新闻网</b></font>
            </a><a href="#">
                <font color="#800000"><b>党史频道</b></font>
            </a></i>
    </div>
    <div class="top02 clear">
        <a href="/">
            <div class="width884"><img src="/today/top01.jpg" width="176" height="226"><img src="/today/top02.jpg" width="197" height="226" alt=" "><img src="/today/top03.jpg" width="191" height="226" alt=" "><img src="/today/top04.jpg" width="185" height="226" alt=" "><img src="/today/top05.jpg" width="135" height="226"></div>
        </a>
    </div>
    <div class="top03 clearfix clear">
        <div class="width884">
            <span><a href="#"><img src="/today/logo.gif" width="134" height="40" alt=" "></a></span>
            <dl class="white">
                <!---->
                <dt>
                    <form id="dateform" name="dateform" method="get" onsubmit="return false;">按日期查找：
                        <select name="smonth" id='smonth'>
                            <option value="">请选择</option>
                            <option value="01">一月</option>
                            <option value="02">二月</option>
                            <option value="03">三月</option>
                            <option value="04">四月</option>
                            <option value="05">五月</option>
                            <option value="06">六月</option>
                            <option value="07">七月</option>
                            <option value="08">八月</option>
                            <option value="09">九月</option>
                            <option value="10">十月</option>
                            <option value="11">十一月</option>
                            <option value="12">十二月</option>
                        </select>
                        <select name="sday" id='sday'>
                            <option value="">请选择</option>
                            <option value="01">一日</option>
                            <option value="02">二日</option>
                            <option value="03">三日</option>
                            <option value="04">四日</option>
                            <option value="05">五日</option>
                            <option value="06">六日</option>
                            <option value="07">七日</option>
                            <option value="08">八日</option>
                            <option value="09">九日</option>
                            <option value="10">十日</option>
                            <option value="11">十一日</option>
                            <option value="12">十二日</option>
                            <option value="13">十三日</option>
                            <option value="14">十四日</option>
                            <option value="15">十五日</option>
                            <option value="16">十六日</option>
                            <option value="17">十七日</option>
                            <option value="18">十八日</option>
                            <option value="19">十九日</option>
                            <option value="20">二十日</option>
                            <option value="21">二十一日</option>
                            <option value="22">二十二日</option>
                            <option value="23">二十三日</option>
                            <option value="24">二十四日</option>
                            <option value="25">二十五日</option>
                            <option value="26">二十六日</option>
                            <option value="27">二十七日</option>
                            <option value="28">二十八日</option>
                            <option value="29">二十九日</option>
                            <option value="30">三十日</option>
                            <option value="31">三十一日</option>
                        </select>
                        <input type="submit" name="Submit" value="查看信息" onclick="go()">
                    </form>
                </dt>
                <form name="searchForm" action='/' method="get" onsubmit="return search()">
                    <!-- <input type="hidden" name="XMLLIST"> -->
                    <dd>搜索：<input type="text" name="names" id="names" style="width:158px; height:20px;" value="<?= $text_search ?>">&nbsp;<input type="submit" value="确定"></dd>
                </form>
            </dl>
        </div>
    </div>
    <!--top end-->
    <!--p1-->
    <div class="p1_content clearfix">
        <div class="width884 p1_01">
            <div class="left">

                <div id="wwdiv"></div>
                <!-- <script type="text/javascript">
                    var wwvars = {
                        monthnum: 1,
                        daynum: 1
                    };
                    swfobject.embedSWF("/img/2010cpc_lishishangdejintian/dangshi.swf?" + Math.random(), "wwdiv", "278", "281", "9.0.0", "", wwvars, false, false);
                </script> -->
                <?php if ($today_top && $today_top['img_name']) : ?>
                    <div class='text-center my-4 ml-3'>
                                <img src="/assets/uploads/<?= $today_top['img_name'] ?>" class='img-thumbnail'>
                            </div>
                        <?php else : ?>
                            <div class='text-center  my-4 ml-3'>
                                <img src="/assets/uploads/20230502115633.jpg"  class='img-thumbnail'>
                            </div>
                <?php endif ?>
            </div>
            <div class="right">
                <h2><img src="/today/p1_01.gif" width="127" height="42" alt=" "></h2>
                <p>
                    “党史上的今天”自2011年1月1日开办以来，受到广大读者的欢迎。截至去年年底，点击量已超过20亿人次，成为庆祝建党90周年网上系列栏目中推出时间最早、持续时间最长、链接网站最多、访问量最高的栏目。<br>　　中国共产党的历史波澜壮阔，学习宣传党的历史任重道远。为满足广大党员、干部、群众和青少年学习中国共产党历史的需要，我们将继续办好这一栏目，每天更新内容，欢迎广大网友继续关注和支持，并对我们的工作提出意见建议。<br>　　网络天地宽又广，党史连着你我他。愿为您服务，让我们同行！
                </p>
            </div>
        </div>
        <!--content-->
        <div class="p1_02 clearfix width884">
            <h1 class="red">
                <?= $text_search?"关于“{$text_search}”的党史":"党史上的今天" ?>
            </h1>

            <?php foreach ($object_list as $v) : ?>
                <div class="px-5 row p1_01">
                    <div class="col-2">
                        <!-- <img src="/assets/uploads/{img_name}" alt="/static/300.png" width="100" height="100"> -->
                        <?php if ($v && $v['img_name']) : ?>
                            <div class='text-center my-4 ml-3'>
                                <img src="/assets/uploads/<?= $v['img_name'] ?>"  class='img-thumbnail'>
                            </div>
                        <?php else : ?>
                            <div class='text-center  my-4 ml-3'>
                                <img src="/static/300.png"  class='img-thumbnail'>
                            </div>
                        <?php endif ?>
                        
                    </div>
                    <div class="col-10">
                        <!-- <h2>{title}</h2> -->
                        <div class='m-4'>
                            <h2><?= $v['title'] ?></h2>
                            <!-- <div>{content}</div> -->
                            <p><?= $v['content'] ?></p>
                        </div>
                    </div>
                    
                </div>
                <!-- {/object_list} -->
            <?php endforeach ?>
            <div class='pager'>
                <!-- {if $pager }
                {!pager!}
                {endif} -->
                <?php
                if (isset($pager)) {
                    echo $pager;
                }
                ?>
            </div>
        </div>
    </div>
    <!--p1 end-->
    <!--copyright-->
    <div class="copyright width884 white clear">人 民 网 版 权 所 有 ，未 经 书 面 授 权 禁 止 使 用<br>
        Copyright &#169; 1997-2013 by www.people.com.cn. all rights reserved</div>
    <!-- <script src="/today/webdig_test.js.下载" language="javascript" type="text/javascript"></script> -->


</div>
<script>
    function go() {
        var smonth = $('select#smonth').val()
        var sday = $('select#sday').val()
        if (!smonth) {
            $('select#smonth').focus()
            return false
        }
        if (!sday) {
            $('select#sday').focus()
            return false
        }
        //    return true

        window.open(`/history/${smonth}${sday}.html`, '_blank')
    }

    function search() {
        var names = $('input#names').val()
        if (names) {
            return true
        }
        $('input#names').focus()
        return false
    }
    $(function() {
        $('#smonth').find("option[value='{vm}']").attr("selected", true)
        $('#sday').find("option[value='{vd}']").attr("selected", true)
    })
</script>