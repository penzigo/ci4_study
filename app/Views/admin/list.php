<div class='container-fluid'>
    <?php $uri = service('uri')?>
    <hr>
    <div class="d-flex justify-content-between">
        <form method="get" action="" class='form-inline' id='searchform'>
            <?php if ($search_fields) : ?>
                <input type="text" name="q" class='form-control' value="<?= get_item($GET, 'q') ?>" id='q' placeholder='<?= join(',', $search_fields) ?>'>
            <?php endif ?>
            <?php if ($filter_fields) : ?>
                <?php foreach ($filter_fields as $key => $value) : ?>
                    <!-- # code... -->
                    <div class="ml-2 input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="<?= $key ?>"><?= $key ?></label>
                        </div>
                        <input type="<?= $value ?>" class='form-control' name="<?= $key ?>" id="<?= $key ?>" placeholder="<?= $key ?>" value="<?= get_item($GET, $key) ?>">
                    </div>
                <?php endforeach ?>
            <?php endif ?>
            <div class="ml-2 input-group" style="width:260px">
                <div class="input-group-prepend">
                    <div class="input-group-text">事件日期</div>
                </div>
                <input type="number" name="month" class='form-control' id="month" placeholder="月" max=12 min=1 value="<?= get_item($GET, 'month') ?>">
                <input type="number" name="day" class='form-control' id="day" placeholder="日" max=31 min=1 value="<?= get_item($GET, 'day') ?>">

            </div>
            <button type="button" class='ml-2 btn btn-outline-info' id='btn_today'>Today</button>
            <button type="submit" class='ml-2 btn btn-outline-info '><abbr title="搜索"><i class="bi bi-search"></i></abbr></button>
            <select class="form-control ml-2" name="pagesize" id="pagesize">
                <option value="10" <?= get_item($GET, 'pagesize') == 10 ? 'selected' : '' ?>>每页10条</option>
                <option value="50" <?= get_item($GET, 'pagesize') == 50 ? 'selected' : '' ?>>每页50条</option>
                <option value="100" <?= get_item($GET, 'pagesize') == 100 ? 'selected' : '' ?>>每页100条</option>
            </select>
        </form>

        <!-- <div class='mx-4'> -->
        <div>
            <a href="/<?= $uri->getPath() ?>" class='btn btn-info'><abbr title="刷新"><i class="bi bi-arrow-repeat"></i></abbr></a>
            <a href="/<?= $uri->getPath() ?>/create" class='btn btn-success'><abbr title="新建"><i class="bi bi-plus-lg"></i></abbr></a>
        </div>
        <!-- </div> -->
    </div>
    <hr>
    <?= view_cell('AlertMessage',  ['type' => 'success', 'message' => 'The user has been updated.']); ?>

    <table class="table table-light table-striped table-hover">
        <thead class="thead-light">
            <tr>
                <?php foreach ($list_display as $item) : ?>
                    <th><?= $item ?></th>
                <?php endforeach; ?>
                <th>Image</th>
                <th>操作</th>
            </tr>

        </thead>
        <tbody>
            <?php foreach ($object_list as $obj) : ?>
                <tr>
                    <?php foreach ($list_display as $item) : ?>
                        <td><?= htmlspecialchars($obj[$item]) ?></td>
                    <?php endforeach; ?>
                    <td>
                        <div class="text-center">
                            <img src="/assets/uploads/<?= $obj['img_name'] ?>" class="img-thumbnail " alt="" style='height:100px'>
                        </div>
                    </td>
                    <td width='160px'>
                        <button class='btn btn-danger item_delete_btn' item_id='<?= $obj[$pk] ?>'>删除</button>
                        <button class='btn btn-primary item_edit_btn' item_id='<?= $obj[$pk] ?>'>编辑</button>
                    </td>

                </tr>
            <?php endforeach; ?>
        </tbody>
        <!-- <tfoot>
        <tr>
            <th>#</th>
        </tr>
    </tfoot> -->
    </table>

    <style>
        div.pager {
            margin-top: 30px;
            text-align: center;
        }

        ul.pagination {
            display: inline-block;

        }

        ul.pagination li {
            float: left;
            /* margin: 0 10px; */
        }

        /* ul.pagination li.active {
            color: red;
        } */
    </style>
    <div class='pager'>
        <!-- <div class='container'> -->
        <!-- <div class='mx-auto'> -->

        <?php if ($pager) {
            # code...
            echo $pager;
        } ?>
        <!-- </div> -->
    </div>
</div>
<script>
    $(function() {
        $('#btn_today').click(function() {
            var time = new Date //获取中国标准时间 var Year = time.getFullYear();//获取当前年份
            var Month = time.getMonth() + 1 //获取当前月份(＋1是因为js中月份是从0开始的)
            var Day = time.getDate() //获取当前几号
            console.log(Month, Day)
            $('#month').val(Month)
            $('#day').val(Day)
            $("#searchform").submit()
        })
        $('.item_delete_btn').click(function() {
            var id = $(this).attr('item_id')
            if (confirm(`是否删除 id = ${id} 的记录?`)) {
                $.ajax({
                    url: `/<?= $uri->getPath() ?>/delete/${id}`,
                    type: "DELETE",
                    // data: $id,
                    success: function(result) {
                        // 请求成功后的回调函数
                        alert(result)
                        location.reload()
                    }
                });
            }

        })
        $('.item_edit_btn').click(function() {
            var id = $(this).attr('item_id')
            window.open(`/<?= $uri->getPath() ?>/update/${id}`, '_self')

        })
        $('ul.pagination').children('li').addClass('page-item')
        $('ul.pagination').children('li').children('a').addClass('page-link')
        $('#pagesize').change(function() {
            $("#searchform").submit()
        })
    })
    // function delete_item(id){
    //     alert(id)
    // }
</script>