<div class="container">
  <!-- <div class="row"> -->
    <!-- <div class="col-12 col-sm8- offset-sm-2 col-md-6 offset-md-3 mt-5 pt-3 pb-3 bg-white from-wrapper"> -->
    <div class="mt-5 p-4 bg-white from-wrapper">
      <!-- <div class="container bg-white my-5 py-3"> -->
        <h3>编辑</h3>
        <hr>
        <div class='row'>

          <div class='col-6'>
            <form class="" method="post">
              <div class="row">
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label for="event_day">事件日期</label>
                    <input type="date" class="form-control" name="event_day" id="event_day" value="<?= set_value('event_day', $object['event_day']) ?>">
                  </div>
                </div>
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label for="title">标题</label>
                    <input type="text" class="form-control" name="title" id="title" value="<?= set_value('title', $object['title']) ?>">
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                    <label for="content">内容</label>
                    <textarea name="content" id="content" cols="30" rows="10" class="form-control"><?= set_value('content', $object['content']) ?></textarea>
                  </div>
                </div>

                <?php if (isset($validation)) : ?>
                  <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                      <?= $validation->listErrors() ?>
                    </div>
                  </div>
                <?php endif; ?>
              </div>

              <div class="row">
                <div class="col-12 col-sm-4">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>

              </div>
          </div>
          <div class='col-6'>
            <?= view_cell('ImageSelector',['image_list'=>$image_list ,'image_path'=>$image_path,'selected_ids'=>$object['avatar']]); ?>

          </div>
        </div>
        </form>
      </div>
    <!-- </div> -->
  <!-- </div> -->
</div>